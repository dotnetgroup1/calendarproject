# CalendarProject
---
## Authors
- Piotr Bednarek 249472
- Marcin Gruchała 248982

---
## Description
Desktop calendar app providing features:
- Event manageing. User is able to add, edit and delete events.   
- Data will be saved automatically in external database.
- Weather API 
---

## Technologies
Project is created with:
- C# 
- WPF
- Entity Framework
- XUnit
- OpenWeather API
- VS Docs - XML
