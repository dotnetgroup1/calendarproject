﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarApplication.Tests
{
    public class WeatherModelTests
    {
        [Xunit.Fact]
        public void convertTime_ShouldConvert()
        {
            //arrange
            System.DateTime expected = new DateTime(2021, 4, 24, 12, 50, 19, System.DateTimeKind.Utc);

            //act
            System.DateTime actual = CalendarApplication.WeatherModel.convertTime(1619268619).ToUniversalTime();
            //assert
            Xunit.Assert.Equal(expected, actual);
        }

        [Xunit.Fact]
        public void tokmk_ShouldReturn() {
            //arrange
            double expected = 72;
            //act
            double actual = CalendarApplication.WeatherModel.tokmh(20);
            //assert
            Xunit.Assert.Equal(expected, actual);
        }
    }
}
