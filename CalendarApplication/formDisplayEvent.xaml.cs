﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace CalendarApplication
{
    /// <summary>
    /// Klasa obsługująca GUI Kalendarza. Jest odpowiedzialna za generowanie siatki dni oraz za logikę programu.
    /// </summary>
    
    public partial class formDisplayAppointment : Window
    {
        /// <item>
        /// <term>listwpDay</term>
        /// <description>Lista przechowująca obiekty typu WrapPanel. Poszczególne obiekty przechowują informację o wydarzeniach w danym dniu. </description>
        /// </item>
        private List<WrapPanel> listwpDay = new List<WrapPanel>();

        /// <item>
        /// <term>currentDate</term>
        /// <description>Zmienna przechowująca dzisiejszą datę w formacie "DateTime" </description>
        /// </item>
        private DateTime currentDate = DateTime.Today;

        /// <item>
        /// <term>is_label_clicked</term>
        /// <description>Zmienna przechowująca informację o tym czy obiekt typu "Label" został naciśnięty. Zmienna jest potrzebna do odróżnienia naciśnięcia 
        /// obketu "Label" oraz "WrapPanel"
        /// </description>
        /// </item>
        private bool is_label_clicked = false;


        /// <item>
        /// <term>is_wrapPanel_clicked</term>
        /// <description>Zmienna przechowująca informację o tym czy obiekt typu "WrapPanel" został naciśnięty. Zmienna jest potrzebna do odróżnienia
        /// naciśnięcia obiektu "Label" oraz "WrapPanel"
        /// </description>
        /// </item>
        private bool is_wrapPanel_clicked = false;


        /// <summary>
        /// Konstruktor GUI aplikacji kalendarza. 
        /// Inicjalizuje wszystkie niezbędne komponenty.
        /// Inicjalizuje połączenie z API pogodowym.
        /// Generuje panel dni oraz wyświetla pełne GUI.
        /// </summary>
        public formDisplayAppointment()
        {
            InitializeComponent();

            APIHandler.InitializeClient();

            GenerateDayGrid(42);
            DisplayCurrentDate();
        }

        /// <summary>
        /// Funkcja obsługująca załadowanie oraz wyświetlenie wszystkich informacji z API pogodowego
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja modyfikuje wartości obiektów typu "Label" odpowiedzialnych za wyświetlanie informacji pogodowych.
        /// </summary>
        private async void loadWeatherInfo() {
            var weatherInfo = await WeatherConnector.GetWeatherInfo();
            if (WeatherConnector.succesful == true)
            {
                lblDescription.Content = $"{weatherInfo.weather[0].description}";
                lblTemp.Content = $"Temp: {weatherInfo.main.temp} °C";
                lblFeelsLike.Content = $"Feels like: {weatherInfo.main.feels_like} °C";
                lblPressure.Content = $"Pressure: {weatherInfo.main.pressure} Pa";
                lblWind.Content = $"Wind: { WeatherModel.tokmh( double.Parse(weatherInfo.wind.speed, CultureInfo.InvariantCulture)) } km/h";
                lblSunRise.Content = $"Sunrise: {WeatherModel.convertTime(Convert.ToDouble(weatherInfo.sys.sunrise)).TimeOfDay}";
                lblSunSet.Content = $"Sunset: {WeatherModel.convertTime(Convert.ToDouble(weatherInfo.sys.sunset)).TimeOfDay}";
            }
            else {
                lblDescription.Content = $"Wrong city name";
                lblTemp.Content = $"";
                lblFeelsLike.Content = $"";
                lblPressure.Content = $"";
                lblWind.Content = $"";
                lblSunRise.Content = "";
                lblSunSet.Content = "";
            }
        }


        /// <summary>
        /// Funkcja odpowiedzialna za dodanie wydarzenia do listy "listwpDay" w celu wyświetlania konkretnych wydarzeń w GUI kalendarza.
        /// Funkcja nie zwraca żadnej wartośći
        /// Funkcja modyfikuje wartości zmiennych w klasie
        /// </summary>
        private void AddNewEvent(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            int dayTag = (int)(sender as WrapPanel).Tag;
            
                if (dayTag != 0)
                {
                    window.txtEvent.Text = "";
                    window.txtDescription.Text = "";
                    window.dtpDate.SelectedDate = new DateTime(currentDate.Year, currentDate.Month, dayTag);
                    window.ShowDialog();
                }
            
            DisplayCurrentDate();
        }


        /// <summary>
        /// Funkcja odpowiedzialna za wyświetlanie szczegółowych informacji o danym wydarzeniu.
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja modyfikuje wartości zmiennych w klasie
        /// </summary>
        private void ShowEventDetails(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            int eventID = (int)(sender as Label).Tag;
            EventsEntities1 db = new EventsEntities1();
            IEnumerable<EventsTable> eventList = db.EventsTables.SqlQuery("select * from EventsTable where Id = @tmp1", new SqlParameter("@tmp1", eventID));
            window.EventID = eventID;

            
            foreach (EventsTable element in eventList)
            {
                window.txtEvent.Text = element.EventName;
                window.txtDescription.Text = element.EventDescription;
                window.dtpDate.SelectedDate = element.EventDate;
                window.ShowDialog();
            }
            DisplayCurrentDate();
        }


        /// <summary>
        /// Funkcja odpowiedzialna za stworzenie siatki dni dla GUI kalendarza. 
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja uzupełnia Listę "listwpDay".
        /// </summary>
        /// <param name="totalDays">
        /// Liczba dni dla których mają zostać wygenerowane "Panele" w siatce dni.
        /// </param>
        private void GenerateDayGrid(int totalDays)
        {
            wpDays.Children.Clear();
            listwpDay.Clear();

            for (int i = 1; i <= totalDays; i++)
            {
                WrapPanel wp = new WrapPanel
                {
                    Name = $"flDay{i}",
                    Width = 157,
                    Height = 111,
                    Background = new SolidColorBrush(Colors.AntiqueWhite),
                    //Background = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
                };

                wp.Cursor = Cursors.Hand;
                wp.MouseLeftButtonUp += new MouseButtonEventHandler(TARGET);


                Separator b = new Separator
                {
                    Height = 110,
                    Width = 1,
                    Foreground = new SolidColorBrush(Colors.Black)
                };

                wpDays.Children.Add(wp);
                wpDays.Children.Add(b);
                listwpDay.Add(wp);
            }
        }


        /// <summary>
        /// Funkcja odpowiedzialna za dodanie wydarzenia do listy "listwpDay" w celu wyświetlania konkretnych wydarzeń w GUI kalendarza.
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja uzupełnia Listę "listwpDay".
        /// </summary>
        /// <param name="startDayAtWpNumber">
        /// Liczba reprezentująca do którego dnia z koleji w miesiącu ma zostać przyppisane wydarzenie. 
        /// </param>
        private void AddEventToWpDay(int startDayAtWpNumber)
        {
            DateTime startDate = new DateTime(currentDate.Year, currentDate.Month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);


            EventsEntities1 db = new EventsEntities1();
            IEnumerable<EventsTable> eventList = db.EventsTables.SqlQuery("select * from EventsTable where EventDate between @tmp1 and @tmp2", new SqlParameter("@tmp1", startDate),   new SqlParameter("@tmp2", endDate) );
            
            foreach(EventsTable element in eventList)
            { 

                DateTime eventDay = element.EventDate;

                Label lbl = new Label
                {
                    Tag = element.Id,
                    Name = "lbl" + element.Id.ToString(),
                    Width = 157,
                    Height = 25,
                    Content = "• " + element.EventName
                };
                lbl.Cursor = Cursors.Hand;
                lbl.MouseLeftButtonUp += new MouseButtonEventHandler(TARGET2);

                listwpDay[eventDay.Day -1 + startDayAtWpNumber - 1].Children.Add(lbl);
            }
        }


        /// <summary>
        /// Funkcja odpowiedzialna za dodanie obiektów typu "Label" do listy "listwpDay" w celu wyświetlania konkretnych dni w GUI kalendarza.
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja uzupełnia Listę "listwpDay".
        /// </summary>
        /// <param name="startDayAtWpNumber">
        /// Liczba reprezentująca do którego dnia z koleji w miesiącu ma zostać przyppisane wydarzenie. 
        /// </param>
        /// <param name="totalDaysInMonth">
        /// Liczba reprezentująca ilość dni w danym miesiącu. 
        /// </param>
        private void AddLabelDayToWpDay(int startDayAtWpNumber, int totalDaysInMonth)
        {

            foreach (WrapPanel wp in listwpDay)
            {
                wp.Children.Clear();
                wp.Background = new SolidColorBrush(Colors.AntiqueWhite);
                wp.Tag = 0;
            }

            for (int i = 1; i <= totalDaysInMonth; i++)
            {
                Label lbl = new Label
                {
                    Name = $"lblDay{i}",
                    Width = 157,
                    Height = 30,
                    Content = i,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 14,
                    FontWeight = FontWeights.Bold
                };

                Separator b = new Separator
                {
                    Height = 1,
                    Width = 157,
                    Foreground = new SolidColorBrush(Colors.Black)
                };

                //PODŚWIETLANIE DZISIEJSZEJ DATY
                if (new DateTime(currentDate.Year, currentDate.Month, i) == DateTime.Today)
                {
                    listwpDay[i - 1 + startDayAtWpNumber - 1].Background = new SolidColorBrush(Colors.Aquamarine);
                }


                //Adding labels
                listwpDay[i - 1 + startDayAtWpNumber - 1].Children.Clear();
                listwpDay[i - 1 + startDayAtWpNumber - 1].Children.Add(lbl);


                //Adding separators
                listwpDay[i - 1 + startDayAtWpNumber - 1].Tag = i;
                listwpDay[i - 1 + startDayAtWpNumber - 1].Children.Add(b);

            }
        }




        /// <summary>
        /// Funkcja zwracająca pierwszy dzień tygodnia dla danego miesiąca.
        /// </summary>
        /// <returns>
        /// Zmienna typu INT reprezentująca pierwszy dzień tygodnia dla danego miesiąca
        /// </returns>
        private int GetFirstDayOfWeekOfCurrentDate()
        {
            DateTime firstDayOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            return (int)(firstDayOfMonth.DayOfWeek + 1);
        }


        /// <summary>
        /// Funkcja zwracająca ilość dni dla danego miesiąca.
        /// </summary>
        /// <returns>
        /// Zmienna typu INT reprezentująca ilość dni dla danego miesiąca.
        /// </returns>
        private int GetTotalDaysOfCurrentDate()
        {
            DateTime firstDayOfCurrentDate = new DateTime(currentDate.Year, currentDate.Month, 1);
            return firstDayOfCurrentDate.AddMonths(1).AddDays(-1).Day;
        }


        /// <summary>
        /// Funkcja wyświetlająca / przeładowywująca wszystkie informacje dla danego miesiąca w GUI kalendarza.
        /// Funkcja nie zwraca żadnej wartości
        /// Funkcja modyfikuje wyświetlane w GUI wartości obiektów takich typu "Label", "WrapPanel",...
        /// </summary>
        private void DisplayCurrentDate()
        {
            CultureInfo culture = new CultureInfo("en-us");
            lblMonthAndYear.Content = currentDate.ToString("MMMM, yyyy", culture);
            AddLabelDayToWpDay(GetFirstDayOfWeekOfCurrentDate(), GetTotalDaysOfCurrentDate());
            AddEventToWpDay(GetFirstDayOfWeekOfCurrentDate());
        }


        /// <summary>
        /// Funkcja wyświetlająca / przeładowywująca wszystkie informacje dla poprzedniego miesiąca w GUI kalendarza. 
        /// Funkcja nie zwraca żadnej wartości
        /// </summary>
        private void PrevMonth()
        {
            currentDate = currentDate.AddMonths(-1);
            DisplayCurrentDate();
        }

        /// <summary>
        /// Funkcja wyświetlająca / przeładowywująca wszystkie informacje dla następnego miesiąca w GUI kalendarza. 
        /// Funkcja nie zwraca żadnej wartości
        /// </summary>
        private void NextMonth()
        {
            currentDate = currentDate.AddMonths(1);
            DisplayCurrentDate();
        }

        /// <summary>
        /// Funkcja wyświetlająca / przeładowywująca wszystkie informacje dla obecnego miesiąca w GUI kalendarza. 
        /// Funkcja nie zwraca żadnej wartości
        /// </summary>
        private void Today()
        {
            currentDate = DateTime.Today;
            DisplayCurrentDate();
        }







        /// <item>
        /// <term>btnNextMonth_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku następnego miesiąca.
        /// Funkcja nie zwraca żadnych wartości.
        /// </description>
        /// </item>
        private void btnNextMonth_Click(object sender, RoutedEventArgs e)
        {
            NextMonth();
        }


        /// <item>
        /// <term>btnPrevMonth_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku poprzedniego miesiąca.
        /// Funkcja nie zwraca żadnych wartości
        /// </description>
        /// </item>
        private void btnPrevMonth_Click(object sender, RoutedEventArgs e)
        {
            PrevMonth();
        }


        /// <item>
        /// <term>Button_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku miesiąca obecnego dnia.
        /// Funkcja nie zwraca żadnych wartości
        /// </description>
        /// </item>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Today();
        }

        /// <item>
        /// <term>btnGetWeather_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku aktualizacji statusu pogody dla danego miasta.
        /// Funkcja nie zwraca żadnych wartości.
        /// </description>
        /// </item>
        private void btnGetWeather_Click(object sender, RoutedEventArgs e)
        {
            WeatherConnector.cityName = txtCity.Text;
            loadWeatherInfo();

        }


        /// <item>
        /// <term>TARGET</term>
        /// <description>
        /// Funkcja odpowiedzialna za sprawdzanie czy naciśnięty został Panel dnia w celu dodania nowego wydarzenia.
        /// Funkcja nie zwraca żadnych wartości.
        /// </description>
        /// </item>
        private void TARGET(object sender, MouseButtonEventArgs e) // Adding new event ( wrap_panel_clicked)
        {
            is_wrapPanel_clicked = true;

            if (is_label_clicked == false && is_wrapPanel_clicked == true)
            {
                AddNewEvent(sender, e);
            }
        }

        /// <item>
        /// <term>TARGET2</term>
        /// <description>
        /// Funkcja odpowiedzialna za sprawdzanie czy naciśnięty został Label wydarzenia w celu jego edycji lub podglądu szczegółów.
        /// Funkcja nie zwraca żadnych wartości.
        /// </description>
        /// </item>
        private void TARGET2(object sender, MouseButtonEventArgs e)// Show event descriptio(label_is_clicked)
        {
            is_label_clicked = true;
            if (is_label_clicked == true && is_wrapPanel_clicked == true)
            {
                ShowEventDetails(sender, e);
            }
            is_label_clicked = false;
        }

    }
}
