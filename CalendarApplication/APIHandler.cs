﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarApplication
{
    /// <summary>
    /// Klasa odpowiedzialna za obsługę zapytań do API.
    /// </summary>
    ///  
    /// <item>
    /// <term>InitializeClient</term>
    /// <description>
    /// Funkcja odpowiedzialna za inizjalizację klasy APIHandler
    /// </description>
    /// </item>
    public class APIHandler
    {
        /// <item>
        /// <term>ApiClient</term>
        /// <description>
        /// Zmienna odpowiedzialna za połączenie HTTPS.
        /// </description>
        /// </item>
        public static System.Net.Http.HttpClient ApiClient { get; set; }


        /// <summary>
        /// Funkcja odpowiedzialna za inizjalizację klasy APIHandler.
        /// Funkcja nie zwraca żadnej wartości
        /// </summary>
        public static void InitializeClient() {
            ApiClient = new System.Net.Http.HttpClient(); 
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
