﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalendarApplication
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <item>
        /// <term>EventID</term>
        /// <description>
        /// Zmienna typu INT przechowująca unikalny numer danego wydarzenia.
        /// </description>
        /// </item>
        public int EventID = 0;
        /// <item>
        /// <term>MainWindow</term>
        /// <description>
        /// Konstruktor okna zarządzania konkretnym wydarzeniem.
        /// </description>
        /// </item>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <item>
        /// <term>btnClose_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku "CLOSE"
        /// Funkcja nie zwraca żadnych wartości
        /// </description>
        /// </item>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <item>
        /// <term>btnSave_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku "SAVE"
        /// Funkcja zapisuje wprowadzone zmiany do istniejącego wydarzenia lub tworzy nowe wydarzenie
        /// Funkcja nie zwraca żanych wartości
        /// </description>
        /// </item>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            EventsEntities1 db = new EventsEntities1();

            if(EventID == 0) // ADDING NEW EVENT
            {
                IEnumerable<EventsTable> eventList = db.EventsTables.SqlQuery("select * from EventsTable");
                int counter = 0;
                foreach (EventsTable element in eventList)
                {
                    counter++;
                }

                EventsTable eventObject = new EventsTable()
                {
                    Id = counter,
                    EventDate = (DateTime)dtpDate.SelectedDate,
                    EventName = txtEvent.Text,
                    EventDescription = txtDescription.Text
                };
                db.EventsTables.Add(eventObject);
            }
            else// EDIT EVENT
            {
                EventsTable event_object = new EventsTable() { Id = EventID };
                event_object.Id = EventID;
                event_object.EventDate = (DateTime)dtpDate.SelectedDate;
                event_object.EventName = txtEvent.Text;
                event_object.EventDescription = txtDescription.Text;
                db.EventsTables.AddOrUpdate(event_object);
            }

            db.SaveChanges();
        }


        /// <item>
        /// <term>Button_Click</term>
        /// <description>
        /// Funkcja odpowiedzialna za reakcję po naciśnięciu przycisku "DELETE"
        /// Funkcja usuwa z bazy danych wydarzenie które zostało wybrane
        /// Funkcja nie zwraca żanych wartości
        /// </description>
        /// </item>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (EventID != 0)
            {
                EventsEntities1 db = new EventsEntities1();
                EventsTable event_object = new EventsTable() { Id = EventID };
                db.EventsTables.Attach(event_object);
                db.EventsTables.Remove(event_object);
                db.SaveChanges();
            }
        }
    }
}
