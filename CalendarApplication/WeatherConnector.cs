﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CalendarApplication
{
    /// <summary>
    /// Klasa obsługująca połaczenie z API pogodowym.
    /// </summary>
    /// 
    /// <item>
    /// <term>GetWeatherInfo</term>
    /// <description>
    /// Funkcja odpowiedzialna za pobranie danych ze strony OpenWeatherMap.org
    /// Funkcja dodatkowo sprawdza czy pobrane dane są poprawne
    /// </description>
    /// </item>
    /// 
    public class WeatherConnector
    {
        
       
        /// <item>
        /// <term>cityName</term>
        /// <description>
        /// Zmienna typu string przechowująca Nazwe miejscowości.
        /// </description>
        /// </item>
        public static string cityName = "";
        /// <item>
        /// <term>succesful</term>
        /// <description>
        /// Zmienna typu bool zawierająca informację czy udało się pobrać informację z API.
        /// </description>
        /// </item>
        public static bool succesful = false;


        /// <summary>
        /// Funkcja odpowiedzialna za pobranie danych ze strony OpenWeatherMap.org
        /// </summary>
        /// <returns>
        /// Instancję klast WeatherModel
        /// </returns>
        public static async Task<WeatherModel> GetWeatherInfo() {
            string url = $"http://api.openweathermap.org/data/2.5/weather?q={cityName}&units=metric&appid=f4aa1bb8544f9251e60dc9bedc30fbfd";

            using (HttpResponseMessage response = await APIHandler.ApiClient.GetAsync(url)) {
                if (response.IsSuccessStatusCode)
                {
                    succesful = true;
                    WeatherModel result = await response.Content.ReadAsAsync<WeatherModel>();

                    return result;

                }
                else {
                    succesful = false;
                    return new WeatherModel();
                    //throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
