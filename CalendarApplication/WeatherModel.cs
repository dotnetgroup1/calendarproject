﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarApplication
{
    /// <summary>
    /// Klasa odpowiedzialna za sczytanie danych z pliku w formacie JSON
    /// </summary>
    public class WeatherModel
    {
        /// 
        /// <item>
        /// <term>weather</term>
        /// <description>
        /// Lista sczytująca obiekt Weather z pliku w formacie JSON
        /// </description>
        /// </item>
        /// 
        public List<Weather> weather { get; set; }

        /// <item>
        /// <term>main</term>
        /// <description>
        /// Obiekt sczytujący obiekt main z pliku w formacie JSON
        /// </description>
        /// </item>
        public Main main { get; set; }

        /// <item>
        /// <term>wind</term>
        /// <description>
        /// Obiekt sczytujący obiekt wind z pliku w formacie JSON
        /// </description>
        /// </item>
        public Wind wind { get; set; }


        /// <item>
        /// <term>sys</term>
        /// <description>
        /// Obiekt sczytujący obiekt sys z pliku w formacie JSON
        /// </description>
        /// </item>
        public Sys sys { get; set; }


        /// <summary>
        /// Klasa reprezentująca obiekt Weather z pliku w formacie JSON
        /// </summary>
       
        public class Weather {
            /// <item>
            /// <term>description</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "description" obiektu Weather z pliku w formacie JSON
            /// </description>
            /// </item>
            public String description { get; set; }
        }



        /// <summary>
        /// Klasa reprezentująca obiekt Main z pliku w formacie JSON
        /// </summary>
        public class Main {
            /// <item>
            /// <term>temp</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "temp" obiektu Main z pliku w formacie JSON
            /// </description>
            /// </item>
            public String temp { get; set; }
            /// <item>
            /// <term>feels_like</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "feels_like" obiektu Main z pliku w formacie JSON
            /// </description>
            /// </item>
            public String feels_like { get; set; }
            /// <item>
            /// <term>pressure</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "pressure" obiektu Main z pliku w formacie JSON
            /// </description>
            /// </item>
            public String pressure { get; set; }
        }

        /// <summary>
        /// Klasa reprezentująca obiekt Wind z pliku w formacie JSON
        /// </summary>
        public class Wind {
            /// <item>
            /// <term>speed</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "speed" obiektu Wind z pliku w formacie JSON
            /// </description>
            /// </item>
            public String speed { get; set; }
        }

        /// <summary>
        /// Klasa reprezentująca obiekt Sys z pliku w formacie JSON
        /// </summary>
        public class Sys {
            /// <item>
            /// <term>sunrise</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "sunrise" obiektu Sys z pliku w formacie JSON
            /// </description>
            /// </item>
            public String sunrise { get; set; }
            /// <item>
            /// <term>sunset</term>
            /// <description>
            /// Zmienna reprezentująca zawartość "sunset" obiektu Sys z pliku w formacie JSON
            /// </description>
            /// </item>
            public String sunset { get; set; }
        }

        /// <summary>
        /// Funkcja konwertująca czas typu UNIX do czasu lokalnego
        /// </summary>
        public static DateTime convertTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        /// <summary>
        /// Funkcja konwertująca jednostkę m/s na km/h
        /// </summary>
        public static double tokmh(double ms) {
            return ms * 3.6;
        }

    }
}
